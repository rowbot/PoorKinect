#include "utils.hpp"
#include "ConfigFile.hpp"

double dist(cv::Point p1, cv::Point p2) {
	return ((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y));
}

std::pair<cv::Point, double> circleFromPoints(cv::Point p1, cv::Point p2, cv::Point p3) {
	double offset = pow(p2.x, 2) + pow(p2.y, 2);
	double bc = (pow(p1.x, 2) + pow(p1.y, 2) - offset) / 2.0;
	double cd = (offset - pow(p3.x, 2) - pow(p3.y, 2)) / 2.0;
	double det = (p1.x - p2.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p2.y); 
	if (abs(det) < TOL) {
		std::cout << "POINTS TOO CLOSE" << std::endl;;  // точки слишком близко
		return std::make_pair(cv::Point(0, 0), 0);
	}

	double idet = 1/det;
	double centerx = (bc * (p2.y - p3.y) - cd * (p1.y - p2.y)) * idet;
	double centery = (cd * (p1.x - p2.x) - bc * (p2.x - p3.x)) * idet;
	double radius = sqrt(pow(p2.x - centerx, 2) + pow(p2.y - centery, 2));

	return std::make_pair(cv::Point(centerx, centery), radius);
}

bool checkTriangleY(double length, double radius, double Ydist, double retLength) {
	return length <= Config::getInstance()->getHandInfoX().RadKLen * radius && Ydist >= Config::getInstance()->getHandInfoX().RadKY * radius && length >= Config::getInstance()->getHandInfoX().KLenght && retLength >= Config::getInstance()->getHandInfoX().KRetLenght && std::max(length, retLength) / std::min(length, retLength) >= Config::getInstance()->getHandInfoX().KMinMax;
}

bool checkTriangleX(double Xdist, double radius, double Ydist) {
	return (Xdist >= Config::getInstance()->getHandInfoY().minKRadX * radius && Xdist <= Config::getInstance()->getHandInfoY().maxKRadX * radius && Xdist < Ydist) || (Ydist >= Config::getInstance()->getHandInfoY().minKRadY * radius && Ydist <= Config::getInstance()->getHandInfoY().maxKRadY * radius && Xdist > Ydist);
}

void printFingers(int num) {
	switch (num) {
		case '0' : {
			std::cout << " ###### " << std::endl;
			std::cout << " ##  ## " << std::endl;
			std::cout << " ##  ## " << std::endl;
			std::cout << " ##  ## " << std::endl;
			std::cout << " ###### " << std::endl;
			break;
		}
		case '1' : {
			std::cout << "    ##  " << std::endl;
			std::cout << "   ###  " << std::endl;
			std::cout << "    ##  " << std::endl;
			std::cout << "    ##  " << std::endl;
			std::cout << "  ######" << std::endl;
			break;
		}
		case '2' : {
			std::cout << " ###### " << std::endl;
			std::cout << " ##  ## " << std::endl;
			std::cout << "    ##  " << std::endl;
			std::cout << "   ##   " << std::endl;
			std::cout << " ###### " << std::endl;
			break;
		}
		case '3' : {
			std::cout << "  ##### " << std::endl;
			std::cout << "     ## " << std::endl;
			std::cout << "    ##  " << std::endl;
			std::cout << "     ## " << std::endl;
			std::cout << "  ##### " << std::endl;
			break;
		}
		case '4' : {
			std::cout << " ##  ## " << std::endl;
			std::cout << " ##  ## " << std::endl;
			std::cout << " ###### " << std::endl;
			std::cout << "     ## " << std::endl;
			std::cout << "     ## " << std::endl;
			break;
		}
		case '5' : {
			std::cout << " ###### " << std::endl;
			std::cout << " ##     " << std::endl;
			std::cout << " ###### " << std::endl;
			std::cout << "     ## " << std::endl;
			std::cout << " ###### " << std::endl;
			break;
		}
		default : std::cerr << "What?" << std::endl;
	}
}

