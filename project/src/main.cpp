#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <opencv2/core.hpp>

#include "utils.hpp"
#include "CommandLine.hpp"

int main(int argc, char *argv[]) {
	Info info(" ", 0, 0);
	if (handleArgs(argc, argv, info) == false) {
		return -1;
	}

	std::cout << "status:" << info.status << std::endl;
	cv::VideoCapture cap;
	if (info.status == 0) {
		cap.open(0);
	}
	if (info.status == 1) {
		cap.open(info.path);
	}
	if (info.status == 2) {
		cap.open(info.fd);
	}

	cv::Ptr<cv::BackgroundSubtractorMOG2> bg = cv::createBackgroundSubtractorMOG2();
	bg->setNMixtures(3);  // сколько кадров пойдет в фон мб?
	bg->setDetectShadows(false);  // установить обнаружение теней на фоне мб?

	cv::namedWindow("Frame");
	cv::namedWindow("Background");
	int backgroundFrame = 500;  // no frames for create normal background

	if (!cap.isOpened()) {
		std::cout << "Open camera faild" << std::endl;
		return -1;
	}
	
	std::vector<std::pair<cv::Point,double> > palm_centers;
	cv::Mat fore;  // mask of frames
	int prvsFinger = 2;
	int col = 1;
	for (;;) {

		// Get the frame
		cv::Mat frame;
		//cap.read(frame);
		cap >> frame;
		if (frame.empty()) {
			break;
		}
		// Update the current background model and get the foreground
		if (backgroundFrame > 0) {  // studing 
			bg->apply(frame, fore);
			backgroundFrame--;
		} else {  // for searching
			bg->apply(frame, fore, 0);
		}

		// Get background image to display it
		cv::Mat back;  // background image
		bg->getBackgroundImage(back);


		// Enhance edges in the foreground by applying erosion and dilation
		cv::erode(fore, fore, cv::Mat());
		cv::dilate(fore, fore, cv::Mat());


		// Find the contours in the foreground
		std::vector<std::vector<cv::Point> > contours;
		findContours(fore, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);  // mask, vector of points vector, only exteme contours, all contours' points
	
		for (int i = 0; i < (int)contours.size(); i++) {  // пробегаемся по всем векторам точек

			// Ignore all small insignificant areas
			if (cv::contourArea(contours[i]) >= 5000) {

				// Draw contour
				std::vector<std::vector<cv::Point> > tcontours;
				tcontours.push_back(contours[i]);
				cv::drawContours(frame, tcontours, -1, cv::Scalar(0, 0, 255), 2);  // кадр, набор точек, все контуры, цвет контура, ширина контуров 2

				// Detect Hull in current contour
				std::vector<std::vector<cv::Point> > hulls(1);
				std::vector<std::vector<int> > hullsI(1);
				cv::convexHull(cv::Mat(tcontours[0]), hulls[0], false);  // т.к. convexHull(tcontours[0], hulls[0]); вернет индексы, а не точки, используем Mat
				/*cv::convexHull(cv::Mat(tcontours[0]), hullsI[0], false);*/  /*!!! */ cv::convexHull(tcontours[0], hullsI[0]);  //- определение выпуклого контура, точки заносятся в hullI
				cv::drawContours(frame, hulls, -1, cv::Scalar(0, 255, 0), 2);

				// Find minimum area rectangle to enclose hand
				cv::RotatedRect rect = cv::minAreaRect(cv::Mat(tcontours[0]));  // оганичиваем все точки контура в прямоугольник

				// Find Convex Defects
				if (hullsI[0].size() > 0) {  // если набор точек пустой, то пропускаем
					cv::Point2f rect_points[4];
					rect.points(rect_points);  // передаем инфу о точках прямоугольника
					for (int j = 0; j < 4; j++) {
						line (frame, rect_points[j], rect_points[(j + 1) % 4], cv::Scalar(255, 0, 0), 1, cv::LINE_8);  // рисуем прям-к 8-канальной линией, уменьшеный в 4 раза
					}

					std::vector<cv::Vec4i> defects;
					cv::convexityDefects(tcontours[0], hullsI[0], defects);  // координаты точек контура, индексы выпуклых точек, вектор выпуклостных дефектов

					cv::Point rough_palm_center;
					if (defects.size() >= 3) {
						std::vector<cv::Point> palm_points;

						for (int j = 0; j < (int)defects.size(); j++) {
							int startidx = defects[j][0];
							cv::Point ptStart(tcontours[0][startidx]);

							int endidx = defects[j][1];
							cv::Point ptEnd(tcontours[0][endidx]);

							int faridx = defects[j][2];
							cv::Point ptFar(tcontours[0][faridx]);

							// Sum up all the hull and defect points to compute average
							rough_palm_center += ptFar + ptStart + ptEnd;  // may be add "(...) / 3;"
							palm_points.push_back(ptFar);
							palm_points.push_back(ptStart);
							palm_points.push_back(ptEnd);
						}

						// Get palm center by 1st getting the average of all defect points, this is the rough palm center,
						// Then U chose the closest 3 points ang get the circle radius and center formed from them which is the palm center.
						rough_palm_center.x /= defects.size() * 3;
						rough_palm_center.y /= defects.size() * 3;

//						cv::Point closest_pt = palm_points[0];  // ближайшая точка к ладони
						std::vector<std::pair<double, int> > distvec;
						for (int i = 0; i < (int)palm_points.size(); i++) {
							double distForCentre = dist(rough_palm_center, palm_points[i]);
							distvec.push_back(std::make_pair(distForCentre, i));
						}
						std::sort(distvec.begin(), distvec.end());  // сортируем по величине радиуса от min for max

						// Keep choosing 3 points till you find a circle with a valid radius
						// As there is a high chance that the closes points might be in a linear line or too close that it forms a very large circle
						std::pair<cv::Point, double> soln_circle;  // ищем валидный радиус круга (не огромный, не мелкий), дуга которого проходит через точки-впадины 
						for (int i = 0; i + 2 < (int)distvec.size(); i++) {
							cv::Point p1 = palm_points[distvec[i + 0].second];
							cv::Point p2 = palm_points[distvec[i + 1].second];
							cv::Point p3 = palm_points[distvec[i + 2].second];
							soln_circle = circleFromPoints(p1, p2, p3);//Final palm center,radius
							if (soln_circle.second != 0) {  // если радиус не ноль, то выходим
								break;
							}
						}

						//	Find avg palm centers for the last few frames to stabilize its centers, also find the avg radius
						palm_centers.push_back(soln_circle);
						if (palm_centers.size() > 10) {
							palm_centers.erase(palm_centers.begin());  // если центров больше 10 удаляем старый
						}
						
						cv::Point palm_center;  // получение среднего радиуса и среднего центра окружности
						double radius = 0;
						for (int i = 0; i < (int)palm_centers.size(); i++) {
							palm_center += palm_centers[i].first;
							radius += palm_centers[i].second;
						}
						palm_center.x /= palm_centers.size();
						palm_center.y /= palm_centers.size();
						radius /= palm_centers.size();

						//Draw the palm center and the palm circle
						//The size of the palm gives the depth of the hand
						int RADIUS_M = 5;
						cv::circle(frame, palm_center, RADIUS_M, cv::Scalar(144, 144, 255), 3);
						cv::circle(frame, palm_center, radius, cv::Scalar(144, 144, 255), 2);

						//Detect fingers by finding points that form an almost isosceles triangle with certain thesholds
						int fingersNum = 0;
						for (int j = 0; j < (int)defects.size(); j++) {  // нахождение пальцев по числу треугольников
							int startidx = defects[j][0];  // ДУБЛИРОВАНИЕ КОДА!!! СТРОКА 211 
							cv::Point ptStart(tcontours[0][startidx]);

							int endidx = defects[j][1];
							cv::Point ptEnd(tcontours[0][endidx]);

							int faridx = defects[j][2];
							cv::Point ptFar(tcontours[0][faridx]);
							//X o--------------------------o Y
							double Xdist = sqrt(dist(palm_center, ptFar));
							double Ydist = sqrt(dist(palm_center, ptStart));
							double length = sqrt(dist(ptFar, ptStart));

							double retLength = sqrt(dist(ptEnd, ptFar));
							if (checkTriangleY(length, radius, Ydist, retLength)) {
								if (std::min(Xdist, Ydist) / std::max(Xdist, Ydist) <= 0.8) {
									if (checkTriangleX(Xdist, radius, Ydist)) {
										cv::line(frame, ptEnd, ptFar, cv::Scalar(0, 255, 0), 1);
										fingersNum++;
									}
								}
							}

						}
						fingersNum = std::min(5, fingersNum);
						//std::cout << "NO OF FINGERS: " << fingersNum << "prvs fingers num: " << prvsFinger << " col : " << col << std::endl;
						if (fingersNum == prvsFinger) {
							col++;
						} else {
							col = 1;
						}
						prvsFinger = fingersNum;
						if (fingersNum == 5 && col > 10) {
							//printFingers(fingersNum);
						}

						// mouseTo(palm_center.x, palm_center.y);//Move the cursor corresponding to the palm
						if (fingersNum < 4) { //If no of fingers is < 4 , click , else release
						//	mouseClick();
						} else {
						//	mouseRelease();
						}
					}
				}
			}
		}
			
		if (backgroundFrame > 0) {
			cv::putText(frame, "Recording Background", cvPoint(30, 30), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(200, 200, 250), 1, CV_AA);
		}
		cv::imshow("Frame", frame);
		cv::imshow("Background", back);
		if (cv::waitKey(10) >= 0) {
			break;
		}
	}
	return 0;
}

