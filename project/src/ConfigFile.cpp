#include "ConfigFile.hpp"

Config::~Config() {  // хотя, судя по тому, что я прочитал на stackoverflow этот деструктор вызываться не будет, но вроде как это решается если сделать метод получения id константным 
	delete myConfig;
}

HandInfoX Config::getHandInfoX() const {
	return handIX;
}

HandInfoY Config::getHandInfoY() const {
	return handIY;
}

Config* Config::myConfig = nullptr;

Config* Config::getInstance() {
	if (!myConfig) {
		myConfig = new Config;
		myConfig->openConfigFile("config");
	}
	return myConfig;
}

int Config::hashit (std::string const& name) {
	if (name == "RadKLen=") {
		return RadKLen_ID;
	} else if (name == "RadKY=") {
		return RadKY_ID;
	} else if (name == "KLenght=") {
		return KLenght_ID;
	} else if (name == "KRetLenght=") {
		return KRetLenght_ID;
	} else if (name == "KMinMax=") {
		return KMinMax_ID;
	} else if (name == "minKRadX=") {
		return minKRadX_ID;
	} else if (name == "maxKRadX=") {
		return maxKRadX_ID;
	} else if (name == "minKRadY=") {
		return minKRadY_ID;
	} else if (name == "maxKRadY=") {
		return maxKRadY_ID;
	} else {
		return -1;
	}
}

bool Config::openConfigFile(const char* path_file) {
	std::ifstream config(path_file);
	try {
		if (!config.is_open()) {
			throw 228;
		}
	} catch (int f) {
		std::cerr << "File wasn't open, code: " << f << std::endl;
		return false;
	}
	for (std::string line; std::getline(config, line); ) {
		std::istringstream in(line);
		std::string name;
		in >> name;
		switch (hashit(name)) {
			case RadKLen_ID : {
				double num = 0.0;
				in >> num; 
				handIX.RadKLen = num;
				break;
			}
			case RadKY_ID : {
				double num = 0.0;
				in >> num; 
				handIX.RadKY = num;
				break;
			}
			case KLenght_ID : {
				double num = 0.0;
				in >> num; 
				handIX.KLenght = num;
				break;
			}
			case KRetLenght_ID : {
				double num = 0.0;
				in >> num; 
				handIX.KRetLenght = num;
				break;
			}
			case KMinMax_ID : {
				double num = 0.0;
				in >> num; 
				handIX.KMinMax = num;
				break;
			}
			case minKRadX_ID : {
				double num = 0.0;
				in >> num; 
				handIY.minKRadX = num;
				break;
			}
			case maxKRadX_ID : {
				double num = 0.0;
				in >> num; 
				handIY.maxKRadX = num;
				break;
			}
			case minKRadY_ID : {
				double num = 0.0;
				in >> num; 
				handIY.minKRadY = num;
				break;
			}
			case maxKRadY_ID : {
				double num = 0.0;
				in >> num; 
				handIY.maxKRadY = num;
				break;
			}
			default : {
				std::cerr << "Unkown constant in config file" << std::endl;
			}
		}
	}
	config.close();
	return true;
}

