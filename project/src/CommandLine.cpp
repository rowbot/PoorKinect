#include "CommandLine.hpp"

bool handleArgs(int argc, char** argv, Info& info) {
	int opt = 0;
	while ((opt = getopt(argc, argv, "v:c:")) != -1) {
		switch (opt) {
			case 'v' : {
				info.path = std::string(optarg);
				info.status = 1;
				break;
			}
			case 'c' : {
				char* end;
				info.fd = strtol(optarg, &end, 10);
				info.status = 2;
				break;
			}
			case '?' : {
				std::cerr << "Usage: -v <path_to_video>  // for hand detect from video" << std::endl;
				std::cerr << "       -c <fd>             // for hand detect from other camera" << std:: endl;
				std::cerr << "       <no_argument>       // for hand detect from webcamera" << std::endl;
				return false;
			}
		}
	}
	return true;
}

