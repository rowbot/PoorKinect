#ifndef HAND_DETECT_CONFIGFILE_HPP_
#define HAND_DETECT_CONFIGFILE_HPP_

#include <fstream>
#include <iostream>
#include <sstream>

struct HandInfoX {
	double RadKLen = 3;
	double RadKY = 0.4;
	double KLenght = 10;
	double KRetLenght = 10;
	double KMinMax = 0.8;
};

struct HandInfoY {
	double minKRadX = 0.1;
	double maxKRadX = 1.3;
	double minKRadY = 0.1;
	double maxKRadY = 1.3;
};



class Config {
 public:
	 enum HandInfoIP {
		 RadKLen_ID,
		 RadKY_ID,
		 KLenght_ID,
		 KRetLenght_ID,
		 KMinMax_ID,
		 minKRadX_ID,
		 maxKRadX_ID,
		 minKRadY_ID,
		 maxKRadY_ID,
	 };

	 HandInfoX getHandInfoX() const;
	 HandInfoY getHandInfoY() const;

	 static Config* getInstance();
	 ~Config();
	 Config& operator =(Config const&) = delete;
	 Config(Config const&) = delete;
	 int hashit (std::string const& name);
	 bool openConfigFile(const char* path);
 private:
	 HandInfoX handIX;
	 HandInfoY handIY;
	 Config() {};  // mb ; don't use
	 static Config* myConfig;
};

#endif  // HAND_DETECT_CONFIGFILE_HPP_

