#ifndef HAND_DETECT_MOUSE_HPP_
#define HAND_DETECT_MOUSE_HPP_

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <iostream> 
#include <algorithm>
#include <cstring>

/*class Mouse {
 public:
	 void mouseClick();
	 void mouseRelease();
	 void mouseTo(int x, int y);

 private:
	 int x;
	 int y;
};*/

void mouseTo(int x, int y);

void mouseClick();

void mouseRelease();

#endif  // HAND_DETECT_MOUSE_HPP_

