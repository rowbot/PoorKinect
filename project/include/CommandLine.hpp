#ifndef HAND_DETECT_COMMANDLINE_HPP_
#define HAND_DETECT_COMMANDLINE_HPP_

#include <iostream>
#include <unistd.h>

struct Info {
	std::string path;
	int fd;
	int status;

	Info(std::string p, int f, int s) : path(p), fd(f), status(s) {};
};

bool handleArgs(int argc, char** argv, Info& info);

#endif  // HAND_DETECT_COMMANDLINE_HPP_

