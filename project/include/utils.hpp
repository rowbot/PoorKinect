#ifndef HAND_DETECT_UTILS_HPP_
#define HAND_DETECT_UTILS_HPP_

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <opencv2/core.hpp>

#include "ConfigFile.hpp"

const double TOL = 0.0000001;

// This function returns the square of the euclidean distance between 2 points.
double dist(cv::Point p1, cv::Point p2);

// This function returns the radius and the center of the circle given 3 points
// If a circle cannot be formed , it returns a zero radius circle centered at (0,0)
std::pair<cv::Point, double> circleFromPoints(cv::Point p1, cv::Point p2, cv::Point p3);

/*class Fingers {
 public:*/
	// Play with these thresholds to improve performance
	bool checkTriangleY(double length, double radius, double Ydist, double retLength);
	
	bool checkTriangleX(double Xdist, double radius, double Ydist);
/*
	Fingers()

 private:
	double Xdist;
	double Ydist;
	double length;
	double retLength;
	int fingersNum;
*/

void printFingers(int num);

#endif  // HAND_DETECT_UTILS_HPP_

