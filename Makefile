TARGET = handDetection.out 
TSTTRG = test.out
CXX = g++
# XXX: Don't forget backslash at the end of any line except the last one

HDRS = \
	   project/include/

SRCS = \
	   project/src/utils.cpp \
	   project/src/Mouse.cpp \
	   project/src/CommandLine.cpp \
	   project/src/ConfigFile.cpp \
	   project/src/main.cpp

TSSRC = \
		test.cpp

.PHONY: all clean test

all: $(SRCS)
	$(CXX) -Wall -Wextra -ggdb $(addprefix -I,$(HDRS)) -o $(TARGET) $(SRCS) `pkg-config --cflags --libs opencv` -lX11 -std=c++17

test: $(TSSRC)
	$(CXX) -Wall -Wextra -ggdb $(addprefix -I,$(HDRS)) -o $(TSTTRG) $(TSSRC) `pkg-config --cflags --libs opencv` -lX11 -std=c++17 

clean:
	rm -rf $(TARGET) $(TSTTRG) *~ project/src/*~ project/include/*~
